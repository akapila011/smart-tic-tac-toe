# Smart Tic Tac Toe

This is a game of tic-tac-toe that allows the player to face off with the computer.
It uses a basic strategic algorithm to ensure that the computer never looses.
The game will only end in 1 of the following 2 scenarios:
* A draw
* The player looses

### Dependencies
* Python 3.x
* PyGame 
**Note: the pygame version installed must match your python version.**

## How to play
You can either download the source code and run the "TicTacToe.py" file **Or**
go to the [downloads](https://bitbucket.org/akapila011/smart-tic-tac-toe/downloads/) page and download the correct version for your machine architecture. **Note:** the downloads only work on Windows machines.

Once the program is running the screen will show the board. Before each game you have to select either Heads or Tails for the coin toss to determine who starts. The winner starts and uses the 'X' symbol. The coin toss helps ensure fairness.

While you play, certains statistics are shown. On the right hand side of the screen you can see the your total wins (human) and right below that is the counter for total wins by the computer. These values refer to the the wins for each session.

![Board Layout](Resources/tictactoe_board_design.jpg "Board Layout")

As you play you the moves made will be recorded on the screen as "Player 1 moves" (your moves) and "AI moves". Each number represents the move made and the order of the moves is maintained. Refer to the image above to see which number corresponds to which board position.

Finally, the bottom of the screen shows whether the game is in progress of "Game Over". The restart button can be clicked at any time to reset the board.

Below is a screenshot of a game that was concluded.

![Finished Game](Resources/smart_tictactoe_screen1.png "Finished Game")

## Future improvements
Possible improvements to be made:
* Replace the module providing logic for the computer to make a move and incorporate a neural net.
* Provide persistent data storage for statistics.
* Slow down computer response time so that it seems more life-like.

