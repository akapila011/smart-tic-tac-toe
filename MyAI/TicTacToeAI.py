#!/usr/bin/env python
# The AI for the Tic Tac Toe game

from random import choice
from os.path import dirname, realpath

# Global Constants to be used for loading/saving wins for each player for each session of play instead of from .csv
HUMAN_WINS = 0
AI_WINS = 0


class AI(object):

    def __init__(self):

        CURRENT_DIR = dirname(realpath(__file__))       # relative path to current file
        self.SAVE_DIR = CURRENT_DIR.replace("MyAI", "save")     # path to save file directory

        self.my_moves = []           # keeps a record move AI makes (1-9)
        self.opposition_moves = []   # a record of moves made by player 1, can be used when we want to save to plays.csv

        self.player1_started = False
        self.strategy_used = False  # is True when a win strategy is used when player 1 starts
        self.smart_block = False   # used to make a pre-determined second move when player uses a starting win strategy

        self.set_moves = [1, 2, 3]      # 3 moves the AI will make instead of random/calculated moves, used for testing

    def save_files_create(self):
        # creates save files (total_wins.csv) if they don't exist
        try:
            f = open(self.SAVE_DIR + "\\" + "total_wins.csv", "w")
            f.write("Human,AI\n")
            f.write("0,0")
            f.close()
        except IOError:
            return None

    def load_wins_record(self):
        # read total_wins.csv and return total wins for AI and human
        try:
            f = open(self.SAVE_DIR + "\\" + "total_wins.csv", "r")
            f.readline()    # skip first line/header line
            wins = f.readline()
            comma_index = wins.index(",")
            human_wins = wins[0:comma_index]
            ai_wins = wins[comma_index + 1::]
            return tuple([human_wins, ai_wins])
        except IOError:
            self.save_files_create()    # create file since it doesn't currently exist
            return tuple([0, 0])    # return 0,0 wins as a fail safe to file reading

    def save_wins_record(self, winner):
        # Updates the total_wins file to show human/AI 1 more win
        try:
            f = open(self.SAVE_DIR + "\\" + "total_wins.csv", "r")  # first read data to memory
            f.readline()    # skip first line/header line
            wins = f.readline()
            comma_index = wins.index(",")
            human_wins = int(wins[0:comma_index])
            ai_wins = int(wins[comma_index + 1::])
            if winner == "AI":      # winner can only be AI or Human
                ai_wins += 1
            else:
                human_wins += 1
            f.close()   # stop reading the file

            f = open(self.SAVE_DIR + "\\" + "total_wins.csv", "w")  # open file to write to
            f.write("Human,AI\n")
            f.write(str(human_wins) + "," + str(ai_wins))
            f.close()   # done writing
        except IOError:
            return None

    def smart_move(self, board, player1_moves, my_moves):
        # UPDATE v 2.0 create self.intelligent_move()
        #   it should run and calculate all moves from the start of the game, as game moves on the choices
        #   should be narrowed and the AI should follow the move that will result in a win or draw as worst case

        # 1. If player starts and makes a move in win positions of 1,3,5,7,9 then block it for future moves
        if len(player1_moves) == 1 and len(my_moves) == 0:
            self.player1_started = True
            return self.smart_start(player1_moves)

        # 2. If AI starts and player move 2 is in any of the edges (2, 4, 6, 8), block the center if not already blocked
        if not self.player1_started and len(player1_moves) == 2 and 5 not in player1_moves and 5 not in my_moves:
            if player1_moves[0] in (2, 4, 6, 8):
                return 5

        # 3. If player 1 uses a corner strategy (move on 2 opposite corners) then block appropriately
        # corner strategy (move on 2 opposite corners)
        # edge strategy move on an edge and second move on opposite side corner
        if self.strategy_used:    # self.strategy_used can only be true if player 1 starts, and made a corner
            self.strategy_used = False
            if player1_moves[0] in (1, 3, 7, 9):    # corner win strategy being used
                move = self.block_corner_strategy_move2(player1_moves)
                if move != 0:
                    return move
            else:                                   # edge win strategy being used
                move = self.block_edge_strategy_move2(player1_moves)
                if move != 0:
                    return move

        # 4. Check if there is a single move that will win the game for AI
        move = self.check_2_similar(board, "P2", "P1")
        if move != 0:    # move is a grid position and not false
            return move

        # 5. Check if there is a move the opponent can make that will win after my move, block it
        move = self.check_2_similar(board, "P1", "P2")
        if move != 0:
            return move

        # 6. check each empty box to see which one will give the greatest possible future moves
        move = self.check_future_moves(board)
        if move != 0:
            return move

        # if all else fails give a random move
        return self.random_move(player1_moves, my_moves)

    def smart_start(self, player1_moves):
        # If player makes a smart start of grid pos 5 or corners (1, 3, 7, 9) then make sure AI first move blocks
        if player1_moves[0] == 5:
            return choice((1, 3, 7, 9))
        self.strategy_used = True   # when player 1 starts in a corner block and edge grid, block center
        return 5

    def block_corner_strategy_move2(self, player1_moves):
        if 1 in player1_moves and 9 in player1_moves:       # player 1 moves at opposite corners 1 and 9
                return choice((2, 4, 6, 8))     # return any edge grid
        if 3 in player1_moves and 7 in player1_moves:       # player 1 moves at opposite corners 1 and 9
            return choice((2, 4, 6, 8))     # return any edge grid
        return 0

    def block_edge_strategy_move2(self, player1_moves):
        # Will never overwrite another move as AI first move will be in grid 5, no return to grid 5
        if player1_moves[0] == 2:
            if player1_moves[1] in (7, 9):
                return choice((1, 3))
            if player1_moves[1] == 4:
                return 1
            if player1_moves[1] == 6:
                return 3
        if player1_moves[0] == 4:
            if player1_moves[1] in (3, 9):
                return choice((1, 7))
            if player1_moves[1] == 2:
                return 1
            if player1_moves[1] == 8:
                return 7
        if player1_moves[0] == 6:
            if player1_moves[1] in (1, 7):
                return choice((3, 9))
            if player1_moves[1] == 2:
                return 3
            if player1_moves[1] == 8:
                return 9
        if player1_moves[0] == 8:
            if player1_moves[1] in (1, 3):
                return choice((7, 9))
            if player1_moves[1] == 4:
                return 7
            if player1_moves[1] == 6:
                return 9
        return 0  # return 0 if all conditions fail, to avoid making this move

    def check_2_similar(self, board, who, opp):
        # Checks if there is a point where there are 2 similar plays made that could end in a win
        # eg a move at grid 1 and grid 3 should return grid 2 is open for a win move
        # all conditions check to ensure the opposing player/AI hasn't played in the free grid
        for row in board:   # check each row
            if row[0] == row[1] and row[0] == who and row[2] != who and row[2] != opp:     # 1 & 2 are similar
                return int(row[2])
            if row[0] == row[2] and row[0] == who and row[1] != who and row[1] != opp:     # 1 & 3 are similar
                return int(row[1])
            if row[1] == row[2] and row[1] == who and row[0] != who and row[0] != opp:     # 2 & 3 are similar
                return int(row[0])
        for col in range(0, 3):     # check each column
            if (board[0][col] == board[1][col] and board[0][col] == who and board[2][col] != who
                 and board[2][col] != opp):    # top 2 similar (eg. 1 & 4)
                return int(board[2][col])
            if (board[0][col] == board[2][col] and board[0][col] == who and board[1][col] != who
                 and board[1][col] != opp):    # top and bottom similar (eg. 1 & 7)
                return int(board[1][col])
            if (board[1][col] == board[2][col] and board[1][col] == who and board[0][col] != who
                 and board[0][col] != opp):    # bottom 2 are similar (eg. 4 & 7)
                return int(board[0][col])
        # Diagonals
        if board[0][0] == board[1][1] and board[0][0] == who and board[2][2] != who and board[2][2] != opp:
            return 9    # 1 & 5 are similar
        if board[0][0] == board[2][2] and board[0][0] == who and board[1][1] != who and board[1][1] != opp:
            return 5    # 1 & 9 are similar
        if board[1][1] == board[2][2] and board[1][1] == who and board[0][0] != who and board[0][0] != opp:
            return 1    # 5 & 9 are similar
        if board[0][2] == board[1][1] and board[0][2] == who and board[2][0] != who and board[2][0] != opp:
            return 7    # 3 & 5 are similar
        if board[0][2] == board[2][0] and board[0][2] == who and board[1][1] != who and board[1][1] != opp:
            return 5    # 3 & 7 are similar
        if board[1][1] == board[2][0] and board[1][1] == who and board[0][2] != who and board[0][2] != opp:
            return 3    # 5 & 7 are similar
        return 0   # return 0 if no 2 similar moves found with an empty space in between

    def check_future_moves(self, board):
        # Finds which grid positions to move into such that it will leave the AI with greatest
        # possible winning moves in future

        empty_grids = {}

        # Find all empty boxes
        for row in board:
            for box in range(0, 3):
                if row[box] != "P1" and row[box] != "P2":
                    empty_grids[row[box]] = 0

        # Check which empty boxes have the most possible moves
        for grid in empty_grids:
            for row in board:       # if grid is in empty row increment possible win lines
                if grid in row and "P1" not in row:
                    empty_grids[grid] += 1
            for col in range(0, 3):
                column = [board[0][col], board[0][col], board[0][col]]
                if grid in column and "P1" not in column:
                    empty_grids[grid] += 1
            if grid in ["1", "5", "9"] and board[0][0] != "P1" and board[1][1] != "P1" and board[2][2] != "P1":
                empty_grids[grid] += 1
            if grid in ["3", "5", "7"] and board[0][2] != "P1" and board[1][1] != "P1" and board[2][0] != "P1":
                empty_grids[grid] += 1

        if len(empty_grids) != 0:
            biggest = int(max(empty_grids, key=empty_grids.get))    # value of max possible moves
            max_moves = []
            for key in empty_grids:         # put all grid numbers with the max value into a list
                if int(empty_grids[key]) == empty_grids[str(biggest)]:
                    max_moves.append(int(key))
            if len(max_moves) > 0:
                a = choice(max_moves)
                return a   # return any random grid number which has a max number of possible moves
            return biggest          # only 1 grid with max possible values, return that grid number
        return 0    # return 0 if no move with future possible win moves

    def random_move(self, player1_moves, my_moves):
        # Make a random move
        self.opposition_moves = player1_moves       # get moves already done for both players
        self.my_moves = my_moves
        move = choice([1, 2, 3, 4, 5, 6, 7, 8, 9])
        while move in self.opposition_moves or move in self.my_moves:
            # don't allow a move in a grid that's already been drawn in, repeat random choice
            move = choice([1, 2, 3, 4, 5, 6, 7, 8, 9])
        return move

    def make_set_moves(self, my_moves):
        # returns pre-set moves (hardcoded), used in testing
        # NOTE: will draw over other moves made
        return self.set_moves[len(my_moves)]


if __name__ == "__main__":
    my_AI = AI()
