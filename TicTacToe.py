#!/usr/bin/env python
# A Tic Tac Toe game that uses and AI
# by AK

import pygame
from random import choice
from MyAI import TicTacToeAI as TTTA

# Global Constants
BLACK = (0, 0, 0)       # (RGB)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY = (153, 153, 153)
SCREEN_WIDTH = 700
SCREEN_HEIGHT = 500


class Player(object):

    def __init__(self):
        # Player will have a record of moves made, toss winner status, game winner status
        # player 1 is user, player 2 is AI
        self.coin_side = None
        self.toss_win = None
        self.player_symbol = None
        self.moves = []
        self.grid_moves = []
        self.won = None

    def add_player_move(self, pos):
        # Check which symbol the player is using then create the appropriate symbol and to moves which will be drawn
        # position received should be center of respective small box to ensure uniformity
        if self.player_symbol == "X":
            self.moves.append(X(pos))
        else:
            self.moves.append(O(pos))
        pass

    def flip_coin(self):
        # player flips a coin and return the result of the toss
        flip_result = choice(["HEADS", "TAILS"])
        return flip_result


class X(object):

    def __init__(self, pos):
        # Object for player move "x" on the board
        self.pos = pos   # position of mouse when clicked
        self.size = 45   # size of x object on the board

    def draw_me(self, screen):
        # diagonal 1, from NW to SE
        pygame.draw.line(screen, BLACK, [self.pos[0] - self.size, self.pos[1] - self.size],
                         [self.pos[0] + self.size, self.pos[1] + self.size], 7)
        # diagonal 2, from SW to NE
        pygame.draw.line(screen, BLACK, [self.pos[0] - self.size, self.pos[1] + self.size],
                         [self.pos[0] + self.size, self.pos[1] - self.size], 7)


class O(object):

    def __init__(self, pos):
        # Object for player move "o" on the board
        self.pos = pos   # position of mouse when clicked
        self.size = 110

    def draw_me(self, screen):
        x = self.pos[0] - (self.size / 2)
        y = self.pos[1] - (self.size / 2)
        # screen, color, [x,y,width,height], thickness
        pygame.draw.ellipse(screen, BLACK, [x, y, self.size, self.size], 7)


class Game(object):

    def __init__(self):

        self.h_t_clicked = False        # check whether heads or tails is clicked (can only be clicked once)
        self.coin_toss_done = False
        self.highlight = None           # used to highlight selected heads or tails
        self.coin_toss_winner = None    # used to update text (in side-panel) telling user who won and starts
        self.whose_turn = None
        self.small_grid_centers = {}    # stores center position for each small grid box to place x and o accordingly
        self.total_moves = 0            # used to see if 9 moves have been made for game over
        self.game_over = False          # used to update game status in side panel
        self.win_info = ()              # holds winner, win type (col/row/diag), row/col no or win grid numbers
        # uncomment line below if self.wins_record is also uncommented, used in an if and changed to true after saving
        # self.win_saved = False       # used to ensure 1 win saved only, otherwise loop will keep incrementing win no
        self.player1 = Player()
        self.player2 = Player()
        self.myAI = TTTA.AI()                # call AI module to be used in making an AI move
        # uncomment line below to use .csv to read wins record and load data to side panel
        # self.wins_record = self.myAI.load_wins_record()     # read save file to get total wins
        self.set_clickable_ranges()     # set ranges for where an action will take place when clicked
        self.set_small_grid_centers()   # set center positions for each small grid box, used to place "x" and "o"
        self.grid_to_list()         # maps grid pos (1-9) to row and column number
        self.board = [["1", "2", "3"], ["4", "5", "6"], ["7", "8", "9"]]  # board to be used in checking for win

    def set_clickable_ranges(self):
        # Method to set ranges where a user can click and a specific action will take place
        # Lists below will be used to determine if the user clicked in an allowed area for the appropriate action
        # e.g. clicking between a certain x value and y value for choosing heads
        # Note: changing heads/tails/restart/board size requires change in range below
        self.heads_x_range = [i for i in range(431, 546)]   # range is rectangle x co-ord, x-co-ord + width
        self.heads_y_range = [i for i in range(60, 100)]
        self.tails_x_range = [i for i in range(558, 673)]
        self.tails_y_range = [i for i in range(60, 100)]
        self.restart_x_range = [i for i in range(498, 618)]
        self.restart_y_range = [i for i in range(453, 493)]
        self.board_x_range = [i for i in range(26, 379)]        # range for playable area i.e. where x and o go
        self.board_y_range = [i for i in range(79, 438)]
        self.grid1_x_range = [i for i in range(30, 128)]        # ranges for each small box, to know where user clicked
        self.grid1_y_range = [i for i in range(80, 178)]
        self.grid2_x_range = [i for i in range(140, 262)]
        self.grid2_y_range = [i for i in range(80, 178)]
        self.grid3_x_range = [i for i in range(272, 375)]
        self.grid3_y_range = [i for i in range(80, 178)]
        self.grid4_x_range = [i for i in range(30, 128)]
        self.grid4_y_range = [i for i in range(190, 315)]
        self.grid5_x_range = [i for i in range(140, 262)]
        self.grid5_y_range = [i for i in range(190, 315)]
        self.grid6_x_range = [i for i in range(272, 375)]
        self.grid6_y_range = [i for i in range(190, 315)]
        self.grid7_x_range = [i for i in range(30, 128)]
        self.grid7_y_range = [i for i in range(328, 435)]
        self.grid8_x_range = [i for i in range(140, 262)]
        self.grid8_y_range = [i for i in range(328, 435)]
        self.grid9_x_range = [i for i in range(272, 375)]
        self.grid9_y_range = [i for i in range(328, 435)]

    def set_small_grid_centers(self):
        # Sets the center position for each small grid so elements are placed uniformly
        self.small_grid_centers[1] = (70, 127)
        self.small_grid_centers[2] = (200, 127)
        self.small_grid_centers[3] = (325, 127)
        self.small_grid_centers[4] = (70, 250)
        self.small_grid_centers[5] = (200, 250)
        self.small_grid_centers[6] = (325, 250)
        self.small_grid_centers[7] = (70, 385)
        self.small_grid_centers[8] = (200, 385)
        self.small_grid_centers[9] = (325, 385)

    def grid_to_list(self):
        # maps grid numbers (1-9) to index positions
        self.grid_to_rc = {1: (0, 0),
                           2: (0, 1),
                           3: (0, 2),
                           4: (1, 0),
                           5: (1, 1),
                           6: (1, 2),
                           7: (2, 0),
                           8: (2, 1),
                           9: (2, 2)
                          }

    def coin_toss_update(self, highlight_button, heads_or_tails):
        # called once head/tails selected
        self.h_t_clicked = True
        self.highlight = highlight_button
        self.coin_toss_done = True
        self.player1.coin_side = heads_or_tails

    def coin_toss_win_update(self, winner, whose_turn, player1_symbol, player2_symbol):
        # called after coin toss to set winner details
        self.coin_toss_winner = winner
        self.whose_turn = whose_turn
        self.player1.player_symbol = player1_symbol
        self.player2.player_symbol = player2_symbol

    def player_make_move(self, which_player, grid_pos):
        self.total_moves += 1                 # update total number of moves played
        if which_player == "Player 1":
            self.player1.add_player_move(self.small_grid_centers[grid_pos])
            self.player1.grid_moves.append(grid_pos)
            self.board[self.grid_to_rc[grid_pos][0]][self.grid_to_rc[grid_pos][1]] = "P1"
            self.whose_turn = "Player 2"    # switch whose turn it is back to AI
        elif which_player == "Player 2":
            self.player2.add_player_move(self.small_grid_centers[grid_pos])
            self.player2.grid_moves.append(grid_pos)
            self.board[self.grid_to_rc[grid_pos][0]][self.grid_to_rc[grid_pos][1]] = "P2"
            self.whose_turn = "Player 1"          # switch whose turn back to user

    def process_events(self):
        # Events from user handled here
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if pos[0] in self.heads_x_range and pos[1] in self.heads_y_range and not self.h_t_clicked:
                    # Check if heads rectangle clicked and whether a head/tail selection was already made
                    self.coin_toss_update("HEADS", "HEADS")
                    # player 2 (AI) flips coin since player 1 made the heads/ tails choice
                    if self.player1.coin_side == self.player2.flip_coin():    # check if player 1 won the coin toss
                        self.coin_toss_win_update("Player 1", "Player 1", "X", "O")
                    else:
                        self.coin_toss_win_update("AI", "Player 2", "O", "X")
                if pos[0] in self.tails_x_range and pos[1] in self.tails_y_range and not self.h_t_clicked:
                    # Check if tails rectangle clicked and whether a head/tail selection was already made
                    self.coin_toss_update("TAILS", "TAILS")
                    # player 2 (AI) flips coin since player 1 made the heads/ tails choice
                    if self.player1.coin_side == self.player2.flip_coin():    # check if player 1 won the coin toss
                        self.coin_toss_win_update("Player 1", "Player 1", "X", "O")
                    else:
                        self.coin_toss_win_update("AI", "Player 2", "O", "X")
                if pos[0] in self.restart_x_range and pos[1] in self.restart_y_range:
                    # Check if restart rectangle was clicked, reset board if clicked
                    self.__init__()
                if (pos[0] in self.board_x_range and pos[1] in self.board_y_range and not self.game_over
                    and self.coin_toss_done and self.whose_turn == "Player 1"):
                    # Check if user is clicking within the board area and only if game is not over (so no additional x
                    # or o is drawn), the coin toss has already reached a result and if it is user's turn
                    if (pos[0] in self.grid1_x_range and pos[1] in self.grid1_y_range
                            and 1 not in self.player1.grid_moves and 1 not in self.player2.grid_moves):   # grid box 1
                        self.player_make_move("Player 1", 1)
                    if (pos[0] in self.grid2_x_range and pos[1] in self.grid2_y_range
                            and 2 not in self.player1.grid_moves and 2 not in self.player2.grid_moves):   # grid box 2
                        self.player_make_move("Player 1", 2)
                    if (pos[0] in self.grid3_x_range and pos[1] in self.grid3_y_range
                            and 3 not in self.player1.grid_moves and 3 not in self.player2.grid_moves):   # grid box 3
                        self.player_make_move("Player 1", 3)
                    if (pos[0] in self.grid4_x_range and pos[1] in self.grid4_y_range
                            and 4 not in self.player1.grid_moves and 4 not in self.player2.grid_moves):   # grid box 4
                        self.player_make_move("Player 1", 4)
                    if (pos[0] in self.grid5_x_range and pos[1] in self.grid5_y_range
                            and 5 not in self.player1.grid_moves and 5 not in self.player2.grid_moves):   # grid box 5
                        self.player_make_move("Player 1", 5)
                    if (pos[0] in self.grid6_x_range and pos[1] in self.grid6_y_range
                            and 6 not in self.player1.grid_moves and 6 not in self.player2.grid_moves):   # grid box 6
                        self.player_make_move("Player 1", 6)
                    if (pos[0] in self.grid7_x_range and pos[1] in self.grid7_y_range
                            and 7 not in self.player1.grid_moves and 7 not in self.player2.grid_moves):   # grid box 7
                        self.player_make_move("Player 1", 7)
                    if (pos[0] in self.grid8_x_range and pos[1] in self.grid8_y_range
                            and 8 not in self.player1.grid_moves and 8 not in self.player2.grid_moves):   # grid box 8
                        self.player_make_move("Player 1", 8)
                    if (pos[0] in self.grid9_x_range and pos[1] in self.grid9_y_range
                            and 9 not in self.player1.grid_moves and 9 not in self.player2.grid_moves):   # grid box 9
                        self.player_make_move("Player 1", 9)
        return False

    def run_logic(self):
        # Updates game logic e.g. where player/ai have made their next move, checks for win and ends game
        if self.total_moves >= 5 and self.total_moves != 10 and not self.game_over:  # need at least 5 moves for a win
            self.win_info = self.check_winner()
            if len(self.win_info) > 0:
                self.game_over = True
                if self.win_info[0][0] == "Player 1":
                    self.player1.won = True
                    self.player2.won = False
                    TTTA.HUMAN_WINS += 1
                else:
                    self.player1.won = False
                    self.player2.won = True
                    TTTA.AI_WINS += 1
        if self.total_moves == 9:
            self.game_over = True
        if self.whose_turn == "Player 2" and not self.game_over:
            # If it's the AI's turn and game is not over then make a move
            move = self.myAI.smart_move(self.board, self.player1.grid_moves, self.player2.grid_moves)
            self.player_make_move("Player 2", move)

    def check_winner(self):
        # check if rows have same values
        # returns a tuple with each winning move since they can be more than 1 eg. a diagonal and column
        # each win move consists of: winner, win type (wo/col/diagonal), winning positions/row no/col no
        px_to_word = {"P1": "Player 1", "P2": "AI"}     # convert acronyms in self.board to words
        win_lines = []
        for row in self.board:
            if row[0] == row[1] and row[1] == row[2]:
                win_lines.append(tuple([px_to_word[row[0]], "row", self.board.index(row) + 1]))
        # check if columns have same values
        for col in range(0, 3):
            if self.board[0][col] == self.board[1][col] and self.board[1][col] == self.board[2][col]:
                win_lines.append(tuple([px_to_word[self.board[0][col]], "col", col + 1]))
        # check first diagonal for win
        if self.board[0][0] == self.board[1][1] == self.board[2][2]:
            win_lines.append(tuple([px_to_word[self.board[0][0]], "diag", (1, 5, 9)]))
        # check second diagonal for win
        if self.board[0][2] == self.board[1][1] == self.board[2][0]:
            win_lines.append(tuple([px_to_word[self.board[0][2]], "diag", (3, 5, 7)]))

        if len(win_lines) > 0:      # if a win move was found then return the win lines
            return tuple(win_lines)
        return tuple([])        # return an empty tuple

    def display_frame(self, screen):
        # Displays all items on the screen
        screen.fill(WHITE)
        self.draw_board(screen)
        self.draw_side_panel(screen)
        self.draw_moves(screen)
        pygame.display.flip()

    def draw_board(self, screen):
        # Draws the tic tac toe board where the 'x' and 'o' will be drawn
        # horizontals
        pygame.draw.line(screen, BLACK, [26, 184], [379, 184], 10)  # screen, color, start_pos[x,y], end_pos, thickness
        pygame.draw.line(screen, BLACK, [26, 322], [379, 322], 10)
        # verticals
        pygame.draw.line(screen, BLACK, [134, 79], [134, 438], 10)
        pygame.draw.line(screen, BLACK, [267, 79], [267, 438], 10)

    def draw_moves(self, screen):
        # draw player and AI moves (X/O) and winning line
        for symbol in self.player1.moves:
            symbol.draw_me(screen)
        for symbol in self.player2.moves:
            symbol.draw_me(screen)
        # draw the winning line only when game over and their is a winner (so not when game restarts)
        if self.game_over and len(self.win_info) > 0:
            for line in self.win_info:
                if line[1] == "row":
                    if line[2] == 1:      # Line through row 1
                        pygame.draw.line(screen, RED, self.small_grid_centers[1], self.small_grid_centers[3], 10)
                    elif line[2] == 2:     # Line through row 2
                        pygame.draw.line(screen, RED, self.small_grid_centers[4], self.small_grid_centers[6], 10)
                    elif line[2] == 3:     # Line through row 3
                        pygame.draw.line(screen, RED, self.small_grid_centers[7], self.small_grid_centers[9], 10)
                elif line[1] == "col":
                    if line[2] == 1:       # Line through column 1
                        pygame.draw.line(screen, RED, self.small_grid_centers[1], self.small_grid_centers[7], 10)
                    elif line[2] == 2:       # Line through column 2
                        pygame.draw.line(screen, RED, self.small_grid_centers[2], self.small_grid_centers[8], 10)
                    elif line[2] == 3:       # Line through column 3
                        pygame.draw.line(screen, RED, self.small_grid_centers[3], self.small_grid_centers[9], 10)
                elif line[1] == "diag":
                    pygame.draw.line(screen, RED, self.small_grid_centers[line[2][0]],
                                     self.small_grid_centers[line[2][2]], 10)

    def draw_side_panel(self, screen):
        # Draw the side panel where all the data is shown
        # is made up of static and dynamic data, dynamic data will be handled by their respective function
        font1 = pygame.font.SysFont("AgencyFB", 35)
        font2 = pygame.font.SysFont("AgencyFB", 24)
        font3 = pygame.font.SysFont("AgencyFB", 28)

        # Main side panel drawings
        pygame.draw.rect(screen, GREY, [411, 0, 290, 500])   # Rectangle background
        # 3 Horizontal Lines separating side panel sections
        pygame.draw.line(screen, BLACK, [411, 157], [700, 157], 8)
        pygame.draw.line(screen, BLACK, [411, 239], [700, 239], 8)
        pygame.draw.line(screen, BLACK, [411, 360], [700, 360], 8)

        # Section 0
        ht_head = font1.render("Heads or Tails", True, BLACK)
        screen.blit(ht_head, [475, 11])
        # If heads or tails clicked highlight selected else draw plain buttons
        self.h_t_buttons(screen)
        self.update_toss_winner(screen)

        # Section 1
        h_wins_label = font2.render("Total Human Wins: ", True, WHITE)
        screen.blit(h_wins_label, [440, 170])
        ai_wins_label = font2.render("Total AI Wins: ", True, WHITE)
        screen.blit(ai_wins_label, [440, 200])
        # h_wins and ai_wins, change first parameter to self.wins_record[0] and [1] respectively when using the .csv
        # file to read wins for each player, self.wins_record and self.win_saved must also be uncommented
        h_wins = font2.render(str(TTTA.HUMAN_WINS), True, BLACK)
        screen.blit(h_wins, [590, 170])
        ai_wins = font2.render(str(TTTA.AI_WINS), True, BLACK)
        screen.blit(ai_wins, [555, 200])

        # Section 2
        player_moves = font2.render("Player 1 Moves: ", True, WHITE)
        screen.blit(player_moves, [420, 250])
        ai_moves = font2.render("AI Moves: ", True, WHITE)
        screen.blit(ai_moves, [420, 300])
        self.screen_update_moves_made(screen)

        # Section 3
        game_status_label = font3.render("Game Status: ", True, WHITE)
        screen.blit(game_status_label, [440, 390])
        game_status = None
        if not self.game_over:
            game_status = font3.render("In Progress", True, WHITE)
        else:
            if len(self.win_info) > 0:
                game_status = font3.render(self.win_info[0][0] + " Won", True, WHITE)
            else:
                game_status = font3.render("Game Over", True, WHITE)
        screen.blit(game_status, [560, 390])
        pygame.draw.rect(screen, BLACK, [498, 453, 120, 40])    # Restart Button
        restart = font2.render("Restart", True, WHITE)
        screen.blit(restart, [530, 455])

    def h_t_buttons(self, screen):
        # Draws the heads and tails buttons and highlights the selected button
        if self.highlight is None:
            pygame.draw.rect(screen, BLACK, [431, 60, 115, 40])     # Heads button
            pygame.draw.rect(screen, BLACK, [558, 60, 115, 40])     # Tails button
        elif self.highlight == "HEADS":
            pygame.draw.rect(screen, BLUE, [431, 60, 115, 40])      # Heads button
            pygame.draw.rect(screen, BLACK, [558, 60, 115, 40])     # Tails button
        elif self.highlight == "TAILS":
            pygame.draw.rect(screen, BLACK, [431, 60, 115, 40])     # Heads button
            pygame.draw.rect(screen, BLUE, [558, 60, 115, 40])      # Tails button
        font2 = pygame.font.SysFont("AgencyFB", 24)
        heads = font2.render("Heads", True, WHITE)
        tails = font2.render("Tails", True, WHITE)
        screen.blit(heads, [465, 66])
        screen.blit(tails, [595, 66])

    def update_toss_winner(self, screen):
        # show coin toss status and winner once completed
        font2 = pygame.font.SysFont("AgencyFB", 24)
        toss_status = None
        if self.coin_toss_winner is None:
            toss_status = font2.render("Coin toss not yet done", True, WHITE)
        else:
            toss_status = font2.render(self.coin_toss_winner + " has won and starts", True, WHITE)
        screen.blit(toss_status, [430, 114])

    def screen_update_moves_made(self, screen):
        # method to update moves made on the side panel
        font = pygame.font.SysFont("AgencyFB", 24)
        # x positions for where text should be placed
        player1_x = 540
        # p1_moves and p2_moves stores moves as pygame text for display
        p1_moves = [font.render(str(move), True, BLACK) for move in self.player1.grid_moves]
        player2_x = 500
        p2_moves = [font.render(str(move), True, BLACK) for move in self.player2.grid_moves]

        for move in p1_moves:           # loop to display text for player1/2
            screen.blit(move, [player1_x, 250])
            player1_x += 15     # spacing between moves displayed in side panel
        for move in p2_moves:
            screen.blit(move, [player2_x, 300])
            player2_x += 15


def main():
    # Initialize pygame
    pygame.init()
    screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
    pygame.display.set_caption("Tic Tac Toe")
    # below 2 lines commented out due to error building executable with pyinstaller
    # icon = pygame.image.load("save\\tictactoe_icon.png")
    # pygame.display.set_icon(icon)
    # Create objects and set the clock
    clock = pygame.time.Clock()
    game = Game()
    done = False
    while not done:
        done = game.process_events()    # 1. Process Events
        game.run_logic()                # 2. Update game data
        game.display_frame(screen)      # 3. Draw current frame
        clock.tick(30)                  # Pause for next frame
    pygame.quit()


if __name__ == "__main__":
    main()
